﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialSpawner : MonoBehaviour
{
    public GameObject[] spawnObjects;

	void Awake()
    {
        
	}

    void Start()
    {
        int waves = 10;
        int z_distance = 5;
        int x_distance = 2;
        float angle = 10;
        for (int i = 1; i < waves; i++)
        {
            for (int j = -3; j <= 3; j += x_distance)
            {
                GameObject spawnObject = spawnObjects[Random.Range(0, spawnObjects.Length)];
                spawnObject.transform.position = new Vector3(
                    j,
                    1 - i * z_distance * Mathf.Tan(Mathf.Deg2Rad * angle),
                    10 + i * z_distance
                );
                Instantiate(spawnObject);
            }
        }
    }
	
	void Update()
    {

	}
}
