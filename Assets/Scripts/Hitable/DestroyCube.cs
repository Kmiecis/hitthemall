﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCube : MonoBehaviour {
    public int scriptID;
    public float velocityMultiply;
    public float gravityMultiply;
    public int cubeScore;

    ScoreController sc;
    CubeController cc;

    void Awake()
    {
        if (velocityMultiply == 0) velocityMultiply = 1;
        if (gravityMultiply == 0) gravityMultiply = 2;

        sc = GameObject.Find("GameController").GetComponent<ScoreController>();
        cc = GameObject.Find("GameController").GetComponent<CubeController>();
    }
    
    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Bound")) {
            if (GetComponent<BoxCollider>().isTrigger)
            {
                GetComponent<BoxCollider>().isTrigger = false;

                GetComponent<Rigidbody>().velocity = other.gameObject.GetComponent<Rigidbody>().velocity * velocityMultiply;
                cc.CubeAwake(scriptID, this.gameObject);

                sc.IncrementScore(cubeScore);
                GameObject.Destroy(this.gameObject);
            }
        }
        
    }
}
