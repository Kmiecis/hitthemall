﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeCube : MonoBehaviour {
    public int scriptID;
    public float explosionVelocityMultiply;
    public float gravityMultiply;
    public int cubeScore;

    ScoreController sc;
    CubeController cc;

    void Awake()
    {
        if (explosionVelocityMultiply == 0) explosionVelocityMultiply = 1;
        if (gravityMultiply == 0) gravityMultiply = 2;

        sc = GameObject.Find("GameController").GetComponent<ScoreController>();
        cc = GameObject.Find("GameController").GetComponent<CubeController>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Bound")) {
            if (GetComponent<BoxCollider>().isTrigger)
            {
                GetComponent<BoxCollider>().isTrigger = false;

                GetComponent<Rigidbody>().velocity = new Vector3(5f, 5f, 5f) * explosionVelocityMultiply;
                cc.CubeAwake(scriptID, this.gameObject);

                sc.IncrementScore(cubeScore);
                GameObject.Destroy(this.gameObject);
            }
        }
        
    }
}