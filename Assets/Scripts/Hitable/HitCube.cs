﻿using UnityEngine;

public class HitCube : MonoBehaviour {
    public float velocityMultiply;
    public float gravityMultiply;
    public int cubeScore;

    Rigidbody rb;
    ScoreController sc;

    bool hit;
    void Awake()
    {
        if (velocityMultiply == 0) velocityMultiply = 1;
        if (gravityMultiply == 0) gravityMultiply = 1;

        rb = GetComponent<Rigidbody>();
        sc = GameObject.Find("GameController").GetComponent<ScoreController>();

        hit = false;
    }

    void FixedUpdate()
    {
        if (hit)
        {
            rb.AddForce(Vector3.down * gravityMultiply);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Bound")) {
            if (GetComponent<BoxCollider>().isTrigger)
            {
                GetComponent<BoxCollider>().isTrigger = false;
                sc.IncrementScore(cubeScore);

                Rigidbody hitByRb = other.gameObject.GetComponent<Rigidbody>();
                rb.velocity = new Vector3(
                    hitByRb.velocity.x * velocityMultiply,
                    (transform.position.y - hitByRb.position.y) * velocityMultiply,
                    hitByRb.velocity.z * velocityMultiply);

                hit = true;
            }
        }
       
    }
}
