﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCubePart : MonoBehaviour {
    public float gravityMultiply;

	void Awake()
    {
        if (gravityMultiply == 0) gravityMultiply = 1;
    }
	
	void FixedUpdate () {
        GetComponent<Rigidbody>().AddForce(Vector3.down * gravityMultiply);
	}

    void OnBecameInvisible()
    {
        this.gameObject.SetActive(false);
    }
}
