﻿using UnityEngine;

public class Rotator : MonoBehaviour {
    Vector3 rotate = new Vector3(0f, 30, 0f);

    public bool Rotate;

    private void Awake()
    {
        
    }

    void Update () {
        if (Rotate) {
            transform.Rotate(rotate * Time.deltaTime);
        }
    }
}
