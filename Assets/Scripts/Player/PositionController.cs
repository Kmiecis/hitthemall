﻿using System.Collections;
using UnityEngine;

public class PositionController : MonoBehaviour {

    private ScoreController scoreController;
    private bool OutOfBounds = false;

    private void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            scoreController = gameControllerObject.GetComponent<ScoreController>();
        }
        if (scoreController == null)
        {
            Debug.Log("Can't find 'GameController' script");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Bound")) {
            OutOfBounds = true;
            StartCoroutine(wait());
            
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bound"))
        {
            OutOfBounds = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Bound"))
        {
            OutOfBounds = false;
        }
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(1);
        CheckPosition();
    }

    private void CheckPosition()
    {
        if (OutOfBounds) {
            scoreController.RestartLevel();
        }
    }

}
