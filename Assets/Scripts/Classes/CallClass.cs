﻿using System;               // Type
using System.Reflection;    // ConstructorInfo, MethodInfo
using UnityEngine;

public class CallClass
{
    Type class_type;
    object class_object;

    public CallClass(string class_name)
    {
        this.class_type = (class_name != null) ? Type.GetType(class_name) : null;
    }

    public bool CallConstructor(object[] parameters)
    {   // gets types of parameters
        Type[] types = new Type[parameters.Length];
        for (int i = 0; i < parameters.Length; i++)
        {
            types[i] = parameters[i].GetType();
        }
        // try to call constructor
        ConstructorInfo class_constructor;
        try { class_constructor = class_type.GetConstructor(types); } catch (Exception) { Debug.Log("Couldn't find this script."); return false; }
        // initiate class object
        class_object = class_constructor.Invoke(parameters);
        return true;
    }

    public bool CallMethod(string method_name, object[] parameters)
    {   // try to call method
        MethodInfo class_method;
        try { class_method = class_type.GetMethod(method_name); } catch (Exception) { Debug.Log("Couldn't find this method."); return false; }
        class_method.Invoke(class_object, parameters);
        return true;
    }
}
