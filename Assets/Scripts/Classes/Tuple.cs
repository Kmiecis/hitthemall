﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuple<T1, T2>
{
    T1 first;
    T2 second;

    public Tuple(T1 first, T2 second)
    {
        this.first = (first != null) ? first : default(T1);
        this.second = (second != null) ? second : default(T2);
    }

    public T1 First
    {
        get { return first; }
        set { first = value; }
    }

    public T2 Second
    {
        get { return second; }
        set { second = value; }
    }

    public override string ToString()
    {
        return string.Format("Tuple({0}, {1})", first, second);
    }
}
