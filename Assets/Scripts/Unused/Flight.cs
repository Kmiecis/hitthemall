﻿using UnityEngine;

/* Class Flight:
 * Sets new Velocity trajectory based on: Rigidbody of object, Rigidbody of object being hit by,
 * and additionally adding some velocity multiplier, to spice things up.
 */

public class Flight
{
    Ray stickRay = new Ray();
    RaycastHit stickHit;
    int stickableMask;

    Rigidbody hitWhatRb;
    Rigidbody hitByRb;
    float veloMultiply;

    public Flight(Rigidbody _hitWhatRb, Rigidbody _hitByRb, float _veloMultiply)
    {
        stickableMask = LayerMask.GetMask("Stickable");
        hitWhatRb = _hitWhatRb;
        hitByRb = _hitByRb;
        veloMultiply = _veloMultiply;
    }

    public Vector3 Velocity()
    {
        stickRay.origin = hitWhatRb.position;
        stickRay.direction = hitByRb.velocity.normalized;

        float range = 9999;
        float velocity_y;

        if (Physics.Raycast(stickRay, out stickHit, range, stickableMask))
        {   // If made 'line' hits something in 'range' and with 'mask' -> Creates 'hit line'
            float height_y = Random.Range(1.5f, 4.5f); // Range of Y at which this Object will land
            float distance = stickHit.distance;
            velocity_y = hitByRb.velocity.magnitude * (height_y / distance);
        }
        else
        {   // Else default 'velocity'
            velocity_y = 2;
        }
        velocity_y = velocity_y * 2 / 2; // Useless line to prevent errors with variable not being used...

        return new Vector3(
            hitByRb.velocity.x * veloMultiply,
            velocity_y = 2,
            hitByRb.velocity.z * veloMultiply);
    }
}