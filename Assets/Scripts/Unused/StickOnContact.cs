﻿using System.Collections;
using UnityEngine;

public class StickOnContact : MonoBehaviour {

	public float waitTillDrop;
    public float dropSpeed;

    private void OnTriggerEnter(Collider other)
    {   
        
        if (other.gameObject.CompareTag("Stickable")) {
            Rotator rotator = other.gameObject.GetComponent<Rotator>();
            rotator.Rotate = false;
            StartCoroutine(wait(other));
        }
    }
 

    IEnumerator wait(Collider other) {
        Rigidbody stickableRb = other.GetComponent<Rigidbody>();
        stickableRb.velocity = new Vector3(0.0f, 0.0f, 0.0f);
        yield return new WaitForSeconds(waitTillDrop);
        stickableRb.velocity = new Vector3(0.0f, -dropSpeed * stickableRb.mass, 0.0f);
    }
}
