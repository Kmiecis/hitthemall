﻿using UnityEngine;

public class DestroyOnWall : MonoBehaviour {

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Stickable") {
            Destroy(other.gameObject);
        }
    }
}
