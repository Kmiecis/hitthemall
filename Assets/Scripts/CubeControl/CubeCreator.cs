﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeCreator
{
    protected GameObject parent;
    protected Divider divider;  /* parent divider */

    public CubeCreator(GameObject parent, int divisions)
    {
        if (divisions <= 0) divisions = 1;
        divider = new Divider(divisions);
        this.parent = parent;
    }

    public CubeCreator(GameObject parent, int divide_x, int divide_y, int divide_z)
    {
        if (divide_x <= 0) divide_x = 1;
        if (divide_y <= 0) divide_y = 1;
        if (divide_z <= 0) divide_z = 1;
        divider = new Divider(divide_x, divide_y, divide_z);
        this.parent = parent;
    }

    protected Vector3 GetScale()
    {
        return new Vector3(
            parent.transform.localScale.x / divider.x,
            parent.transform.localScale.y / divider.y,
            parent.transform.localScale.z / divider.z
        );
    }

    protected Quaternion GetRotation()
    {
        return parent.transform.rotation;
    }

    protected Vector3 GetPosition(int i) {
        Vector3 particleDefaultPositon = new Vector3(
            parent.transform.position.x - parent.transform.localScale.x / 2,
            parent.transform.position.y - parent.transform.localScale.y / 2,
            parent.transform.position.z - parent.transform.localScale.z / 2
        );

        int x = 1; int y = 1; int z = 1;
        while (i > divider.z * divider.z && z < divider.z)
        { 
            i -= divider.z * divider.z; z++;
        }
        while (i > divider.y && y < divider.y)
        {
            i -= divider.y; y++;
        }
        x = i;

        Point A = new Point(parent.transform.position.x, parent.transform.position.z);
        Point B = new Point(particleDefaultPositon.x + GetScale().x * (x - 0.5f), particleDefaultPositon.z + GetScale().z * (z - 0.5f));
        Point C = new RotatedPoint(A, B, parent.transform.eulerAngles.y);
        Vector3 particlePosition = new Vector3(
            C.a,
            particleDefaultPositon.y + GetScale().y * (y - 0.5f),
            C.b
        );

        return particlePosition;
    }

    protected virtual Vector3 GetVelocity(Vector3 particlePosition) { return new Vector3(); }

    ///Material particle_material;
    public void ProcessObject(GameObject particle, int i)
    {
        particle.transform.localScale = GetScale();
        particle.transform.rotation = GetRotation();
        particle.transform.position = GetPosition(i);
        particle.GetComponent<Rigidbody>().velocity = GetVelocity(particle.transform.position);
        ///particle_go.GetComponent<Renderer>().material = particle_material;
        particle.SetActive(true);
    }
}

public class Divider
{
    public int x, y, z;
    int iter;

    public Divider(int divisions) {
        x = y = z = 1;
        iter++;

        int i = 2;
        while (i <= divisions)
        {
            if (divisions % i == 0)
            {
                NextValue(i); divisions /= i; i = 2;
            }
            else i++;
        }
    }

    public Divider(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        iter++;
    }

    void NextValue(int _v)
    {
        switch(iter)
        {
            case 1:
                x *= _v;
                iter++;
                break;
            case 2:
                y *= _v;
                iter++;
                break;
            case 3:
                z *= _v;
                iter = 1;
                break;
            default:
                Debug.Log("Divider ERROR: iterator manipulated -> out_of_reach");
                break;
        }
    }
}

class RotatedPoint : Point
{
    Point A;
    Point B;
    float angle;
    // also inherits 'a' and 'b' from Point
    public RotatedPoint(Point _A, Point _B, float _angle)
    {
        A = _A; B = _B; angle = _angle;
        SetPoint();
    }

    void SetPoint()
    {
        float dist = Mathf.Sqrt(Mathf.Pow(B.a - A.a, 2) + Mathf.Pow(B.b - A.b, 2));

        angle *= Mathf.Deg2Rad;
        angle += Mathf.Atan2(B.a - A.a, B.b - A.b);

        a = A.a + dist * Mathf.Sin(angle);
        b = A.b + dist * Mathf.Cos(angle);
    }
}

class Point
{
    public float a;
    public float b;
    
    public Point() { this.a = 0f; this.b = 0f; }
    public Point(float a) { this.a = a;  this.b = 0f; }
    public Point(float a, float b) { this.a = a; this.b = b; }
}