﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeDestroyer : CubeCreator
{
    public CubeDestroyer(GameObject parent, int divisions) : base(parent, divisions) { }

    protected override Vector3 GetVelocity(Vector3 particlePosition)
    {
        Vector3 particleVelocity = new Vector3(  /* velocity vector */
            particlePosition.x - parent.transform.position.x + parent.GetComponent<Rigidbody>().velocity.x,
            particlePosition.y - parent.transform.position.y + parent.GetComponent<Rigidbody>().velocity.y,
            particlePosition.z - parent.transform.position.z + parent.GetComponent<Rigidbody>().velocity.z
        );
        particleVelocity = particleVelocity.normalized * parent.GetComponent<Rigidbody>().velocity.magnitude;

        return particleVelocity;
    }
}
