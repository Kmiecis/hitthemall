﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeExploder : CubeCreator
{
    public CubeExploder(GameObject parent, int divisions) : base(parent, divisions) { }

    protected override Vector3 GetVelocity(Vector3 particlePosition)
    {
        Vector3 particleVelocity = new Vector3(  /* velocity vector */
            particlePosition.x - parent.transform.position.x,
            particlePosition.y - parent.transform.position.y,
            particlePosition.z - parent.transform.position.z
        );
        particleVelocity = particleVelocity.normalized * parent.GetComponent<Rigidbody>().velocity.magnitude;

        return particleVelocity;
    }
}

