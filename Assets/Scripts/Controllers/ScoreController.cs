﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {
    public Text scoreText;
    public Text winText;

    void Awake()
    {
        score = 0;
        SetScoreText();

        winText.text = "";
    }

    int score;

    void SetScoreText()
    {
        scoreText.text = "Score: " + score.ToString();
    }

    public void IncrementScore(int _score)
    {
        score += _score;
        SetScoreText();
    }

    public int getScore()
    {
        return score;
    }

    public void RestartLevel() {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    public void NextLevel() {
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (sceneIndex == SceneManager.sceneCountInBuildSettings - 1)
        {
            SceneManager.LoadScene(sceneIndex);
        }
        else {
            SceneManager.LoadScene(sceneIndex + 1);
        }
        
    }
    
}
