﻿using UnityEngine;

public class CameraController : MonoBehaviour {
    public GameObject player;

    Vector3 offset;

    void Awake()
    {
        if (player == null) Debug.Log(string.Format("Missing: GameObject player in CameraController."));
    }

	// Use this for initialization
	void Start ()
    {
        offset = transform.position - player.transform.position;
	}
	
	// LateUpdate is called once per frame but after everything was performed
	void LateUpdate ()
    {
        transform.position = player.transform.position + offset;
	}
}
