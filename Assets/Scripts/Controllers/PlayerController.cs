﻿using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float speed_multiplier;

    void Awake()
    {
        speed_multiplier = (speed_multiplier <= 0) ? 1 : speed_multiplier;
    }
    
    Vector2 reference_point = new Vector2(0.0f, 0.0f);     // first touch position
    void FixedUpdate()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                reference_point = touch.position;
            }
            else if ((touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary))
            {
                Vector2 delta_position = touch.position - reference_point;
                Vector3 movement = new Vector3(delta_position.x, 0.0f, delta_position.y);
                GetComponent<Rigidbody>().velocity += movement.normalized * 0.1f * speed_multiplier;
            }
        }
    }
}
