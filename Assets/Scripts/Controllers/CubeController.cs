﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

[System.Serializable]
public class Editable
{
    public string scriptName;
    public GameObject cube;
    public int count;
}

public class CubeController : MonoBehaviour {
    public Editable[] initialized;
    int multiplier = 2;                                     // initializing number of cubes as 'count' times this value

    Dictionary<int, List<GameObject>> cubes;    // int -> script id; List<GameObject> -> list of cubes; int -> number of inactive cubes

    void Awake()
    {
        cubes = new Dictionary<int, List<GameObject>>();
    }
        
    void Start()
    {
        Initialize();
    }

    void Initialize(int script_id = 0)
    {   // filling 'cubes' with GameObject's
        foreach (var script in initialized)
        {
            cubes.Add(script_id, new List<GameObject>());  // should be in try-catch block, but 'key' value is itself incrementing
            for (int i = 0; i < initialized[script_id].count * multiplier; i++)
            {   // adding cubes until desired quantity is obtained
                AddCube(ref script_id);
            }
            script_id++;
        }
    }

    void AddCube(ref int script_id)
    {   // adds cube
        cubes[script_id].Add(Instantiate(initialized[script_id].cube));
    }

    public bool CubeAwake(int script_id, GameObject parent)
    {   // sets cube particles based on script id and object 'parent' properties (eg. position, velocity, etc...)
        if (!CheckInput(ref script_id)) return false;

        CallClass CC = new CallClass(initialized[script_id].scriptName);
        CC.CallConstructor(new object[] { parent, initialized[script_id].count });

        int n = 1;
        foreach (GameObject particle in cubes[script_id])
        {
            if (!particle.activeSelf)
            {
                CC.CallMethod("ProcessObject", new object[] { particle, n }); n++;
            }
            if (n > initialized[script_id].count)
                break;
        }

        int i = cubes[script_id].Count - 1;
        while (i != 0 && !cubes[script_id][i].activeSelf)
            i--;
        while (initialized[script_id].count > cubes[script_id].Count - i)
        {   // make sure that there is enough cubes for later use by this script
            AddCube(ref script_id);
        }

        return true;
    }

    bool CheckInput(ref int script_id)
    {
        if (initialized.Length <= script_id)
        {
            Debug.Log(string.Format("script_id: {0} - script id does not exist.", script_id)); return false;
        }
        if (initialized[script_id].cube == null)
        {
            Debug.Log(string.Format("script_id: {0} - cube field is empty.", script_id)); return false;
        }
        if (initialized[script_id].count <= 0)
        {
            Debug.Log(string.Format("script_id: {0} - wrong count number.", script_id)); return false;
        }
        return true;
    }
}
