﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FinishingLevel : MonoBehaviour {

    public Text winText;
    public int maxScore;

    private ScoreController scoreController;

    private void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            scoreController = gameControllerObject.GetComponent<ScoreController>();
        }
        if (scoreController == null)
        {
            Debug.Log("Can't find 'GameController' script");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) {
            if (scoreController.getScore() < maxScore)
            {
                StartCoroutine(Lost());
            }
            else {
                StartCoroutine(Win());
            }
                
        }
    }

    IEnumerator Win()
    {
        winText.text = "Win!";
        yield return new WaitForSeconds(1);
        scoreController.NextLevel();
    }

    IEnumerator Lost()
    {
        winText.text = "Lost :(";
        yield return new WaitForSeconds(1);
        scoreController.RestartLevel();
    }
}
